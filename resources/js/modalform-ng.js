var assetTracking = angular.module('assetTracking',[]);
assetTracking.controller('mapController',function($scope){
    $scope.floorSrc = 'resources/svgfiles/not_available.jpg';
    $scope.floors=[];
    $scope.floorSwitch = {};
    $scope.asset = {};
    //This function will initialize floors data when page loads.
    $scope.loadFloors = function(){
        $scope.floorsObj = localStorage.getItem('floors');
        $scope.floors = $scope.floorsObj ? JSON.parse($scope.floorsObj) : [];
        if($scope.floors.length == 0) {
            var floor = {};
            floor.name = "Floor-1";
            floor.floorSrc = "resources/svgfiles/floor1.svg";
            floor.id = 1;
            floor.coordinates = [];
            $scope.floors.push(floor);
            floor = {};
            floor.name = "Floor-2";
            floor.floorSrc = "resources/svgfiles/floor4.svg";
            floor.id = 2;
            floor.coordinates = [];
            $scope.floors.push(floor);
            localStorage.setItem("floors", JSON.stringify($scope.floors));


            floor = {};
            floor.name = "Floor-3";
            floor.floorSrc = "resources/svgfiles/3.png";
            floor.id = 3;
            floor.coordinates = [];
            $scope.floors.push(floor);
            localStorage.setItem("floors", JSON.stringify($scope.floors));

            floor = {};
            floor.name = "Floor-4";
            floor.floorSrc = "resources/svgfiles/4.png";
            floor.id = 4;
            floor.coordinates = [];
            $scope.floors.push(floor);
            localStorage.setItem("floors", JSON.stringify($scope.floors));

            floor = {};
            floor.name = "Floor-5";
            floor.floorSrc = "resources/svgfiles/5.png";
            floor.id = 5;
            floor.coordinates = [];
            $scope.floors.push(floor);
            localStorage.setItem("floors", JSON.stringify($scope.floors));
        }

    }

    //Get floor image based on selected floor
    $scope.getFloorImg = function(){
        $scope.floorSrc = 'resources/svgfiles/not_available.jpg';
        angular.forEach($scope.floors, function(floor){
            if(floor.id === parseInt($scope.floorSwitch.id)){
                $scope.floorSrc= floor.floorSrc;
            }
        });
        loadIcons();
        $scope.populateSearch();
    };

    //Initialization of floor and image
    $scope.init = function(){
        $scope.loadFloors();
        $scope.floorSwitch = $scope.floors[0];
        $scope.getFloorImg();
    };

    //Group and populate search content based on selected floor
    $scope.populateSearch = function(){
        $scope.categoryDropdown = [];
        //Checks floor availability
        if($scope.floors.length) {
            angular.forEach($scope.floors,function(fValue,fIndex){
                //Iterates floor from floors json and checks against selected floor from dropdown
                if(fValue.id === $scope.floorSwitch.id) {
                    angular.forEach(fValue.coordinates, function (value, index) {
                        var found = false;
                        angular.forEach($scope.categoryDropdown, function(cdValue,cdIndex){
                            //Grouping condition
                            if(cdValue.category == value.category){
                                cdValue.coordinates.push(value);
                                found = true;
                            }
                        });
                        if(!found) {
                            var obj = {};
                            obj.category = value.category;
                            obj.coordinates = [];
                            obj.coordinates.push(value);
                            $scope.categoryDropdown.push(obj);
                        }
                    });
                }
            });
        }
    };

    $scope.populateSearch();

    //Function to display markers based on category which user selects.
    $scope.showCategoryMarkers=function (category){
        var obj = {};
        obj.category = category;
        loadIcons(obj);
    }

    //Function to display markers based on UUID which user selects.
    $scope.showMarker= function(uuid){
        var obj = {};
        obj.uuid = uuid;
        loadIcons(obj);
    }

    //This function will be called when search box text changes.
    $scope.resetIcon = function(){
        loadIcons();
        $('.collapse').removeClass('show');
    }

    $scope.validate = function(){
        var isNotValid = false;
        if($scope.asset.name == undefined || $scope.asset.name == '') {
            isNotValid = true;
            $scope.asset.errorMessage = 'Name is mandatory';
        }else if($scope.asset.year != undefined && ($scope.asset.year <1000 || $scope.asset.year>9999)) {
            isNotValid = true;
            $scope.asset.errorMessage = 'Year installed should between 1000 and 9999';
        }else if($scope.asset.unitCost != undefined && $scope.asset.unitCost <0) {
            isNotValid = true;
            $scope.asset.errorMessage = 'Unit cost should be positive value';
        }
        return isNotValid;

    }

    $scope.resetAssetForm = function(){
        $scope.asset = {};
        $scope.asset.category = 'Architectural';
        $scope.asset.name='';
        $scope.asset.code='';
        $scope.asset.year=undefined;
        $scope.asset.unitCost = undefined;
    }
    $scope.resetAssetForm();

    //This watch functionality added to monitor changes in localstage item floors.
    $scope.$watch(function(){
        return localStorage.getItem('floors');
    },function(newVal,oldVal){
        loadIcons();
        $scope.floorsObj = newVal;
        $scope.floors = $scope.floorsObj ? JSON.parse($scope.floorsObj) : [];
        $scope.populateSearch();
    });
    $scope.clearSearch = function(){
        $scope.searchText = '';
    }
});
