assetTracking.controller('filterController',function($scope){
    $scope.filterObj = {
        field:'YI',
        operation:'GT'
    };

    $scope.filterIt = function(){
        var filterObj = $scope.filterObj;
        var $iconsMap = $("#icons-map");
        $iconsMap.html('');
        var floors = getFloorJsonFromStorage();
        angular.forEach(floors, function(floor){
            if(parseInt($("#floor-switch option:selected").val()) == floor.id) {
                angular.forEach(floor.coordinates, function (coord) {
                    if(filterObj.field == 'YI'){
                        if(filterObj.operation === 'GT' && coord.year > filterObj.value){
                            pinMarker($iconsMap, coord);
                        } else if(filterObj.operation === 'LT' && coord.year < filterObj.value){
                            pinMarker($iconsMap, coord);
                        } else if(filterObj.operation === 'EQ' && coord.year == filterObj.value){
                            pinMarker($iconsMap, coord);
                        }else if(filterObj.operation === 'BETWEEN' && (coord.year >= filterObj.value && coord.year <= filterObj.evalue)){
                            pinMarker($iconsMap, coord);
                        }
                    }
                    else if(filterObj.field == 'UC'){
                        if(filterObj.operation === 'GT' && coord.unitCost > filterObj.value){
                            pinMarker($iconsMap, coord);
                        } else if(filterObj.operation === 'LT' && coord.unitCost < filterObj.value){
                            pinMarker($iconsMap, coord);
                        } else if(filterObj.operation === 'EQ' && coord.unitCost == filterObj.value){
                            pinMarker($iconsMap, coord);
                        }else if(filterObj.operation === 'BETWEEN' && (coord.unitCost >= filterObj.value && coord.unitCost <= filterObj.evalue)){
                            pinMarker($iconsMap, coord);
                        }
                    }
                    $("#" + coord.uuid).click(onClickIcon);
                });
            }
        })
    };

    $scope.resetIcons = function(){
        loadIcons();
    };
});
