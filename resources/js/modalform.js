/**
 * This function generates UUID
 * @returns {string}
 */
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

/**
 * Finds color based on asset category
 * @param category
 * @returns {string}
 */
function getColor(category) {
    var color;
    switch (category) {
        case 'Architectural' :
            color = "#00ff00";
            break;
        case 'Electrical' :
            color = "#ff0000";
            break;
        case 'Mechanical' :
            color = "#0000ff";
            break;
        default :
            color = "#00ff00";
            break;
    }

    return color;
}

/**
 * find and convert json string from location storage to json object
 * @returns {any | []}
 */
function getFloorJsonFromStorage() {
    var floors = localStorage.getItem('floors'),
        floorsJSON = floors ? JSON.parse(floors) : [];
    return floorsJSON;
}

/**
 * Find coordinates by UUID
 * @param uuid
 * @returns {*}
 */
function findIconById(uuid) {
    var floorsJSON = getFloorJsonFromStorage();
    for (var i = 0; i < floorsJSON.length; i++) {
        for (var j = 0; j < floorsJSON[i].coordinates.length; j++) {
            if (floorsJSON[i].coordinates[j].uuid === uuid) {
                return floorsJSON[i].coordinates[j];
            }
        }
    }
}

/**
 * Pins the markers based on coordinates info
 * @param element
 * @param coordinate
 */
function pinMarker(element, coordinate) {
    element
        .append("<div id='" + coordinate.uuid +
            "' style='top:" + coordinate.y + "px;left:" + coordinate.x +
            "px; color: " + getColor(coordinate.category) + "' class='icon fa fa-map-marker'><span style='color: black'>" + coordinate.name + "</span></div>"
        );
}

/**
 * Loads markers based on the condition & selected floor
 * 1. If condition is not available then display all available coordinates from localstorage
 * 2. If condition contains UUID then display respective marker.
 * 3. If condition contains category then displays all available markers for the category
 * @param condition
 */
function loadIcons(condition) {
    var $iconsMap = $("#icons-map");
    $iconsMap.html('');
    var floorsJSON = getFloorJsonFromStorage();
    for (var i = 0; i < floorsJSON.length; i++) {
        if (parseInt($("#floor-switch option:selected").val()) === floorsJSON[i].id) {
            for (var j = 0; j < floorsJSON[i].coordinates.length; j++) {
                if (condition == undefined) {
                    pinMarker($iconsMap, floorsJSON[i].coordinates[j]);
                } else if ((condition.uuid === floorsJSON[i].coordinates[j].uuid) ||
                    (condition.category === floorsJSON[i].coordinates[j].category)) {
                    pinMarker($iconsMap,floorsJSON[i].coordinates[j]);
                    animateIcon(floorsJSON[i].coordinates[j]);
                }
                $("#" + floorsJSON[i].coordinates[j].uuid).click(onClickIcon);
            }
        }
    }
}

/**
 * Animate Icon to highlight when searched.
 * @param coordinate
 */
function animateIcon (coordinate) {
    var count=0;
    var x = setInterval(function(){
        $('#' + coordinate.uuid).toggleClass('hightlight-icon');
        if(count > 2) clearInterval(x);
        count++;
    },1000);
}

/**
 * Save/update the asset information to localstorage
 */
function iconSave() {
    if(!angular.element($("#mapController")).scope().validate()) {
        var color = getColor($("#category").val()),
            coordUuid = $("#icon-uuid").val(),
            floor = {};
        var foundFloor = false,
            foundCoord = false;
        var floorsJSON = getFloorJsonFromStorage();

        for (var i = 0; i < floorsJSON.length; i++) {
            if (parseInt($("#floor-switch option:selected").val()) === floorsJSON[i].id) {
                floor = $.extend({}, floorsJSON[i]);
                break;
            }
        }

        $('#' + coordUuid).css('color', color);
        $('#' + coordUuid + '-text').html($("#name").val());

        floor.coordinates.push({
            name: $("#name").val(),
            category: $("#category").val(),
            code: $("#code").val(),
            x: $("#map-x").val(),
            y: $("#map-y").val(),
            uuid: coordUuid,
            year: $("#yearInstalled").val(),
            unitCost: $("#unitCost").val()
        });


        for (i = 0; i < floorsJSON.length; i++) {
            if (floorsJSON[i].id === floor.id) {
                for (var j = 0; j < floorsJSON[i].coordinates.length; j++) {
                    if (floorsJSON[i].coordinates[j].uuid === coordUuid) {
                        floorsJSON[i].coordinates[j].name = $("#name").val();
                        floorsJSON[i].coordinates[j].code = $("#code").val();
                        floorsJSON[i].coordinates[j].category = $("#category").val();
                        floorsJSON[i].coordinates[j].year = $("#yearInstalled").val();
                        floorsJSON[i].coordinates[j].unitCost = $("#unitCost").val();
                        foundCoord = true;
                        break;
                    }
                }
                if (!foundCoord) {
                    floorsJSON[i].coordinates.push({
                        name: $("#name").val(),
                        category: $("#category").val(),
                        code: $("#code").val(),
                        x: $("#map-x").val(),
                        y: $("#map-y").val(),
                        uuid: coordUuid,
                        year: $("#yearInstalled").val(),
                        unitCost: $("#unitCost").val()
                    });
                }
                foundFloor = true;
                break;
            }
        }
        if (!foundFloor) {
            floorsJSON.push(floor);
        }
        localStorage.setItem("floors", JSON.stringify(floorsJSON));
        //save floor in localstorage
        $('form')[1].reset();
        $('#myModal').modal('toggle');
        angular.element($("#mapController")).scope().resetAssetForm();
    }else{
        console.log('toasted');
        $('.toast').toast('show');
    }
}

//Populates model data and display Asset info for update.
function onClickIcon(e) {
    var coordinate = findIconById(e.target.id);
    if (coordinate) {
        $("#name").val(coordinate.name);
        $("#category").val(coordinate.category);
        $("#code").val(coordinate.code);
        $("#icon-uuid").val(coordinate.uuid);
        $("#yearInstalled").val(coordinate.year);
        $("#unitCost").val(coordinate.unitCost);
        $("#myModal").modal();
        //below code to update angularjs model value
        $('input').trigger('input');
        $('input').trigger('change');
    }
   // $('form')[1].reset();
}

/**
 * Removes empty marker when user clicks close button.
 */
function onModalClose() {
    var coordinate = findIconById($("#icon-uuid").val());
    if (coordinate == undefined) {
        $("#" + $("#icon-uuid").val()).remove();
    }
    $('form')[1].reset();
}

/**
 * Image Click to add asset icon.
 * @param e
 */
function imageClick (e) {
    var coordUuid = uuidv4();
    $("#icons-map")
        .append("<div id='" + coordUuid + "' style='top:" + e.pageY + "px;left:" + e.pageX  + "px;' class='icon fa fa-map-marker'><span id='" + coordUuid + "-text' style='color: black'></span></div>");
    $("#map-x").val(e.pageX);
    $("#map-y").val(e.pageY);
    $("#icon-uuid").val(coordUuid);
    $(".myObj").addClass("circle");
    $("#myModal").modal();
    $("#" + coordUuid).click(onClickIcon);
}


/**
 * Starting point of JQuery
 */
$(document)
    .ready(
        function (e) {
            $('.toast').toast('hide');
            loadIcons();
            $("#image").click(imageClick);
            $("#modal-submit").click(iconSave);
        });